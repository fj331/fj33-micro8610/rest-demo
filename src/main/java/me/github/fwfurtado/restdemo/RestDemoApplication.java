package me.github.fwfurtado.restdemo;

import me.github.fwfurtado.restdemo.AddressClient.Address;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RestDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestDemoApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(AddressClient client) {
        return args -> {
            Address address = client.findByZipCode("04101300");

            System.out.println(address);
        };
    }
}
