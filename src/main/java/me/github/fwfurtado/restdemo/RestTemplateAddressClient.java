package me.github.fwfurtado.restdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

//@Component
public class RestTemplateAddressClient implements AddressClient {

    private final RestTemplate rest;
    private final String viaCepURITemplate;

    public RestTemplateAddressClient(RestTemplate rest, @Value("${address-api.zipcode-template}") String viaCepURITemplate) {
        this.rest = rest;
        this.viaCepURITemplate = viaCepURITemplate;
    }

    @Override
    public Address findByZipCode(String zipCode) {
        return rest.getForObject(viaCepURITemplate, Address.class, zipCode);
    }
}
