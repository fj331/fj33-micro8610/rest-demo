package me.github.fwfurtado.restdemo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "address", url = "${address-api.base}")
public interface AddressClient {

    @GetMapping("{zipCode}/json")
    Address findByZipCode(@PathVariable("zipCode") String zipCode);

    @Getter
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    @AllArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    @ToString
    class Address {

        @JsonProperty("logradouro")
        private String street;

        @JsonProperty("uf")
        private String state;

        @JsonProperty("cep")
        private String zipCode;
    }
}
