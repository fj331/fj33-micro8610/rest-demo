package me.github.fwfurtado.restdemo;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.ResponseEntity.accepted;
import static org.springframework.http.ResponseEntity.noContent;

import com.fasterxml.jackson.databind.util.ArrayBuilders.BooleanBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {


    @GetMapping("{id}")
    BookView show(@PathVariable int id) {
        BookView book = new BookView(id, "Spring Cloud", 329);

        Link selfLink = linkTo(methodOn(BookController.class).show(id)).withSelfRel();
        Link updateLink = linkTo(methodOn(BookController.class).updateBy(id)).withRel("update");
        Link removeLink = linkTo(methodOn(BookController.class).deleteBy(id)).withRel("remove");

        book.add(selfLink);
        book.add(updateLink);
        book.add(removeLink);

        return book;
    }

    @PutMapping("{id}")
    ResponseEntity<?> updateBy(@PathVariable int id) {
        return accepted().build();
    }

    @DeleteMapping("{id}")
    ResponseEntity<?> deleteBy(@PathVariable int id) {
        return noContent().build();
    }

    @Getter
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    @AllArgsConstructor
    static class BookView extends RepresentationModel<BookView> {
        private int id;
        private String title;
        private int numberOfPages;
    }

}
